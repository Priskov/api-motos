import { Injectable, ɵɵresolveBody } from '@angular/core';
import { Moto } from '../interfaces/interfaces';


@Injectable({
  providedIn: 'root'
})

export class DataService {
  

   moto:Moto


  constructor() { }

  
  setMoto(moto:Moto){
    this.moto= moto
  }

  getMoto(){
    return this.moto
  }
}