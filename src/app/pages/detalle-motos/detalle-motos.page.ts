import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Moto } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-detalle-motos',
  templateUrl: './detalle-motos.page.html',
  styleUrls: ['./detalle-motos.page.scss'],
})
export class DetallePage implements OnInit {

  moto:Moto

  constructor(private data:DataService,private router:Router, private alertController: AlertController) { }

  ngOnInit() {
    this.moto = this.data.getMoto()
    
    if(!this.moto){
      this.router.navigate(['motos'])
    }
  }

  delete(){
    this.borrarMoto();
  }

  async deleteMoto(id:number){
    const info=await fetch(`http://unai-prisco-7e3.alwaysdata.net/miapi/motos/${id}`,{
      method:"DELETE"
    })
    return info
  }

  async borrarMoto(){
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Cuidado!',
      message: 'Estás seguro de que deseas eliminar esta moto?',
      buttons: [
        {
          text: 'NO',
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: 'SI',
          handler: () => {
            this.deleteMoto(this.moto.id);
            this.router.navigate(['/motos']);
          }
        }
      ]
    });

    await alert.present();
  }

  }


