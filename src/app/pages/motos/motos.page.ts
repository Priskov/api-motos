import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Moto } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';



@Component({
  selector: 'app-motos',
  templateUrl: './motos.page.html',
  styleUrls: ['./motos.page.scss'],
})
export class HomePage implements OnInit {

  motos:any
  moto:Moto
  filtrar:string

  pages=[
    {
      title:'Ducati',
      foto:"https://1000marcas.net/wp-content/uploads/2019/12/Ducati-logo.png"
    },
    {
      title:'Yamaha',
      foto:"https://e7.pngegg.com/pngimages/875/92/png-clipart-yamaha-motor-company-yamaha-corporation-motorcycle-logo-motorcycle-company-logo.png"
    },
    {
      title:'Honda',
      foto:"https://upload.wikimedia.org/wikipedia/commons/7/7b/Honda_Logo.svg"
    },
    {
      title:'Todas',
      foto:"https://www.vhv.rs/dpng/d/230-2300828_icono-moto-png-png-download-car-motorcycle-png.png"
    }
  ]

  constructor(private data: DataService ,private router:Router) { 
    
  }

  ngOnInit() {
    (async ()=>{
      await this.getMotos()
    })()
  }


  ionViewDidEnter(){
    (async ()=>{
      this.motos=await this.getMotos()
    })()
  }

  async getMotos(){
    const info=await (await fetch(`http://unai-prisco-7e3.alwaysdata.net/miapi/motos`)).json()
    return info
  }

  
  async filtrarMarcas(marca:string){
    const info=await (await fetch(`http://unai-prisco-7e3.alwaysdata.net/miapi/motos/filtrar?marca=${marca}`)).json() 
    return info
  }

  filtrarMotos(marca){

    (async () => {
      if(marca!='Todas'){
        this.motos= await this.filtrarMarcas(marca)
      } 
      else this.motos= await this.getMotos()
    })()
  }



  addMoto(){
    this.router.navigate(['alta-moto'])
  }
  
  detalle(moto){
    this.data.setMoto(moto)
    this.router.navigate(['detalle'])

  }

}
