import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-alta-moto',
  templateUrl: './alta-moto.page.html',
  styleUrls: ['./alta-moto.page.scss'],
})
export class AltaMotoPage implements OnInit {


  marca: string;
  modelo: string;
  year: string;
  precio: number;
  foto: any;



  constructor(private router: Router) { }

  ngOnInit() {
  }

  async postMoto(form){
    console.log(form)
    const info=await fetch("http://unai-prisco-7e3.alwaysdata.net/miapi/fotos",{
      method:'POST',
      body:form
    })
    return info
  }


  addMoto() {
    
    this.foto = (<HTMLInputElement>document.getElementsByName("foto")[0]).files[0];
    const stringPrecio = this.precio + "";
    const form = new FormData();

    form.append('marca',this.marca)
    form.append('modelo',this.modelo)
    form.append('year',this.year)
    form.append('foto',this.foto)
    form.append('precio', stringPrecio)

    this.postMoto(form);
    this.router.navigate(['/motos']);

   
  }

}
