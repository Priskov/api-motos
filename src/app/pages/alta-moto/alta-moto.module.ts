import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AltaMotoPageRoutingModule } from './alta-moto-routing.module';

import { AltaMotoPage } from './alta-moto.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AltaMotoPageRoutingModule
  ],
  declarations: [AltaMotoPage]
})
export class AltaMotoPageModule {}
