import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'motos',
    pathMatch: 'full'
  },
  {
    path: 'motos',
    loadChildren: () => import('./pages/motos/motos.module').then( m => m.HomePageModule)
  },
  {
    path: 'detalle',
    loadChildren: () => import('./pages/detalle-motos/detalle-motos.module').then( m => m.DetallePageModule)
  },
  {
    path: 'alta-moto',
    loadChildren: () => import('./pages/alta-moto/alta-moto.module').then( m => m.AltaMotoPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
