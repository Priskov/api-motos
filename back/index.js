const express = require("express"); 
const cors = require('cors'); 
const bodyParser = require('body-parser'); 
var multer  = require('multer');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, '../uploads')
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname + '' )
    }
  })
   
  var upload = multer({ storage: storage })
  
const baseUrl = '/miapi';
const app = express();
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false })); 
app.use(bodyParser.json());

ddbbConfig = {
   user: 'unai-prisco-7e3',
   host: 'postgresql-unai-prisco-7e3.alwaysdata.net',
   database: 'unai-prisco-7e3_motos',
   password: 'itbcat123',
   port: 5432
};

const Pool = require('pg').Pool
const pool = new Pool(ddbbConfig);

const getMotos = (request, response) => {
   var consulta = "SELECT * FROM  motos ORDER BY modelo ASC"
   pool.query(consulta, (error, results) => {
       if (error) {
           throw error
       }
       response.status(200).json(results.rows)
       console.table(results.rows);
   });
}

const getMotosFiltered = (request, response) => {
   var marca = request.query.marca;
   console.log(request.query.marca);
   var consulta = `SELECT * FROM  motos WHERE marca = '${marca}'`
   pool.query(consulta, (error, results) => {
      if (error) {
          throw error
      }
      response.status(200).json(results.rows)
      console.table(results.rows);
   });
}

const deleteMotos = (request, response) => {
   var consulta = `DELETE FROM motos WHERE id=${request.params.id}`
   pool.query(consulta, (error, results) => {
      if (error) {
          throw error
      }
      response.send("Eliminada")
      console.log("Moto eliminada")
   });
}


 app.post(baseUrl+'/fotos', upload.single('foto'), function (req, res, next) {
    req.file.path = req.file.originalname;
    var insert="INSERT INTO motos VALUES(default,'"+req.body.marca+"','"+req.body.modelo+"','"+req.body.year+"','http://unai-prisco-7e3.alwaysdata.net/uploads/" + req.file.originalname+"','"+req.body.precio+"');";

    pool.query(insert, (error, results) => {
        if (error) {
            throw error
        }else{
        }
        
    });
    const file = req.file
  if (!file) {
    const error = new Error('Please upload a file')
    error.httpStatusCode = 400
    return next(error)
  }
    res.send(file)
  });

app.get(baseUrl + '/motos', getMotos);
app.get(baseUrl + '/motos/filtrar', getMotosFiltered);
app.delete(baseUrl + '/motos/:id', deleteMotos);


const PORT = process.env.PORT || 3000; 
const IP = process.env.IP || null; 

app.listen(PORT, IP, () => {
   console.log("El servidor está inicialitzat en el puerto " + PORT);
});
